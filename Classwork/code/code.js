/*Классная работа
Создайте функцию-конструктор Calculator, который создаёт объекты с тремя методами:
read() запрашивает два значения при помощи prompt и сохраняет их значение в свойствах объекта.
sum() возвращает сумму введённых свойств.
mul() возвращает произведение введённых свойств.
*/


function Calculator () {
    this.read = function () {
    this.a = +prompt("Введите первое число"); /*Свой-ва прототипа*/
    this.b = +prompt("Введите второе число"); /*Свой-ва прототипа*/
     };
 };
 

Calculator.prototype.sum = function () { /*Создаём прототип, который с помощью функции возвращаеет сумму*/
    return this.a + this.b;
};

Calculator.prototype.mul = function () { /*Создаём прототип, который с помощью функции возвращаеет сумму*/
    return this.a * this.b;
};

Calculator.prototype.sub = function () { /*Создаём прототип, который с помощью функции возвращаеет сумму*/
    return this.a - this.b;
};

Calculator.prototype.div = function () { /*Создаём прототип, который с помощью функции возвращаеет сумму*/
    return this.a / this.b;
};


const calculation = new Calculator ();
calculation.read();

document.write("<h3> Значения, которые возвращает объект, используя прототип </h3>")
document.write("<span style='color: blue';> Сумма: </span>" + calculation.sum() + "<br>")
document.write("<span style='color: blue';> Умножение  : </span>" + calculation.mul() + "<br>")
document.write("<span style='color: blue';> Отнимание  : </span>" + calculation.sub() + "<br>")
document.write("<span style='color: blue';> Деление  : </span>" + calculation.div() + "<br>")