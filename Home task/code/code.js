/*Домашнее задание*/
/*Задание 1*/
/*Разработайте фун-ю-конструктор, котора\ будет создавать объект Human (человек). 
Создайте массив объектов и реализуйте фун-ю, которая будет сортировать эл-ты мас-ва 
по значению свойства Age по возрастанию или убыванию.*/
/*
   Функция-конструктор персоны
   * @{string} name - имя
   * @{number} age - возраст
  */
   function Human(name, age) {
    this.name = name;
    this.age = age;
  };
 var Ken = new Human('Ken', 28);
 var Pol = new Human('Pol', 22);
 var Maria = new Human('maria', 74);
 var July = new Human('july', 25);
 var Bill = new Human('Bill', 36);
 //массив всех персон
 var peoples = [Ken, Pol, July, Bill, Maria];
 /*
  Функция сортировки по возрасту
  * @{object} mas - массив персон
 */
 function sortByAge(mas) {
 		//Проверка параметра на массив
		if (mas instanceof Array) { 
    	var sortedData = mas.sort(func)
    	return sortedData;
    } else console.log('Error');  
 
		function func(a, b) {
    	return a.age - b.age;
    };
 };
 /*
  * Функция сортировки по имени
  * @{object} mas - массив персон
 */
 function sortByName(mas) {
 		//Проверка параметра на массив
		if (mas instanceof Array) { 
    	var sortedData = mas.sort(func)
    	return sortedData;
    } else console.log('Error'); 
    function func(a, b) {
      return a.name.toUpperCase() > b.name.toUpperCase();
    };
 };
/*
	* Функция рендера данных
  * @{object} mas - массив объектов
  * @{string} key - item объекта
*/
 function render(mas, item) {
 	mas.forEach(show);
  function show(el, i) {
  	document.body.textContent +=' '+(el[item]);
  };
 };
 
 render(sortByName(peoples), 'name');
 render(sortByAge(peoples), 'age');



/*Задание 2*/
/*Разработайте фун-ю-конструктор, котора\ будет создавать объект Human (человек). 
Добавьте на своё усмотрение свой-ва и методы в этот объектю
Подумайте, какие методы и свойства следует сделать уровня экземпляр, а какие уровня фун-ции-конструктора. */

function Humannew (name, surname, age) {
  this.name = name;
  this.surname = surname;
  this.age = age;
  this.sayHi = function () {
  return ("Привіт, я - " + name + " " + surname + ". Мені " + age + ". ");
  } ;
}
//створення екземпляра   
var student = new Humannew("Олексій", "Красовський",21);

console.log(student.sayHi());

//наслідування ознак прототипу та додавання ознак екземпляру
let worker = new Humannew("Єгор", "Красовський", 28);
worker.speciality = "Front-End Developer";
worker.sayHiNew = function () {
return (this.sayHi() + "Моя спеціалізація:" + this.speciality);
};

console.log(worker.sayHiNew());
